const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
  
  books.forEach(book => {
    try {
      if (!book.author) {
        throw new Error("В об'єкта немає властивості 'author'");
      }
      if (!book.name) {
        throw new Error("В об'єкта немає властивості 'name'");
      }
      if (!book.price) {
        throw new Error("В об'єкта немає властивості 'price'");
      }
      
      const listItem = document.createElement('li');
      listItem.textContent = `${book.author}: ${book.name} - ${book.price} грн`;
      bookList.appendChild(listItem);
    } catch (error) {
      console.error(error.message);
    }
  });

  // При виконанні запитів до віддаленого сервера може виникнути різноманітні помилки, такі як втрата з'єднання 
  // або неправильна відповідь сервера. Використання try...catch дозволяє обробити ці помилки і забезпечити гладку роботу додатку, уникнувши збоїв.